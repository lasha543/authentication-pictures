package com.example.picturesauthentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_first.*

class FirstActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()
    }
    private fun init(){
        loginButton.setOnClickListener{
            login()

        }

    }
    private fun login(){
        val email = eMailMadeText.text.toString()
        val password = passWordMadeText.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty()){
            progressBarLogin.visibility= View.VISIBLE
            loginButton.isClickable=false
            val intent= Intent(this,SecondActivity::class.java)
            startActivity(intent)
            if(email.isEmpty() && password.isEmpty() ) {
                Toast.makeText(this,"Please Fill all Fields", Toast.LENGTH_SHORT).show()
            }


        }

                }






}





