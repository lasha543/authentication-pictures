package com.example.picturesauthentication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_layout.view.*

class RecycleViewAdapter(private val iters:MutableList<String>):RecyclerView.Adapter<RecycleViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_layout,parent,false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return iters.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()


    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val imageUrl = iters[adapterPosition]
            itemView.imageView
            Glide
                .with(itemView.context )
                .load(imageUrl)
                .placeholder(R.mipmap.ic_launcher)



        }

    }

}